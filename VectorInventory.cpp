#include <iostream>
#include <vector>
#include <string>
#include <time.h>

using namespace std;

// Build a vector of pre-defined items.
// Returned value doesn't need to be referenced. The caller of the function will be responsible for the created vector.
vector<string> createInventory(int size);
// Count the number of instances of the given item in the inventory.
// 'inventory' is a const reference since we only need read access.
int countItem(const vector<string>& inventory, string item);
// Remove a specific item in the inventory.
// 'inventory' is not a const reference since we need read and write access.
// Optional. Returns true if an item was successfuly removed
bool removeItem(vector<string>& inventory, string item);
// Print the contents of the vector
// 'inventory is a const reference since we only need read access
void printInventory(const vector<string>& inventory);


vector<string> createInventory(int size)
{
	// Define all possible items
	string itemPool[] =
	{
		"RedPotion",
		"Elixir",
		"EmptyBottle",
		"BluePotion"
	};

	// Fill the inventory with random items
	vector<string> items;
	for (int i = 0; i < size; i++)
	{
		// Get a random item from the pool
		string randomItem = itemPool[rand() % 4];

		// Put the random item in the inventory
		items.push_back(randomItem);
	}

	return items;
}

int countItem(const vector<string>& inventory, string item)
{
	// Count items
	int count = 0;
	for (int i = 0; i < inventory.size(); i++)
	{
		// Increase count if it's the same item
		if (inventory[i] == item) count++;
	}

	return count;
}

bool removeItem(vector<string>& inventory, string item)
{
	// We use -1 as default value. This represents that we haven't found the item in the vector.
	int index = -1;

	// Determine the index of the item. We only need to look for the 'first instance' in case of duplicates.
	for (int i = 0; i < inventory.size(); i++)
	{
		// If we found the item, set the index and exit out of the loop. We only need to remove the first instance.
		if (inventory[i] == item)
		{
			index = i;
			break;
		}
	}

	// Remove the item from the vector if we found one (using the index above)
	if (index != -1)
	{
		inventory.erase(inventory.begin() + index);
		return true;
	}

	return false;
}

void printInventory(const vector<string>& inventory)
{
	cout << "--------INVENTORY----------" << endl;
	for (int i = 0; i < inventory.size(); i++)
	{
		cout << inventory[i] << endl;
	}
	cout << "---------------------------" << endl << endl;
}

int main()
{
	srand(time(0));

	// Create the inventory (10 items)
	vector<string> inventory = createInventory(10);

	// Print the inventory
	printInventory(inventory);

	// Count a given item based on user input
	string itemToCount;
	cout << "Input item to count: ";
	cin >> itemToCount;

	int count = countItem(inventory, itemToCount);
	cout << "Found " << count << " " << itemToCount << endl;
	cout << endl;

	// Remove an item based on user inpute
	string itemToRemove;
	cout << "Input item to remove: ";
	cin >> itemToRemove;

	bool success = removeItem(inventory, itemToRemove);
	if (success)
	{
		cout << itemToRemove << " removed." << endl;
	}
	else
	{
		cout << itemToCount << " does not exist." << endl;
	}

	// Print the inventory again
	cout << endl;
	printInventory(inventory);
}
